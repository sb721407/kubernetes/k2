## SkillBox DevOps. Инфраструктурная платформа на основе Kubernetes. Часть 2. Знакомство с Kubernetes: основные понятия и архитектура

SkillBox DevOps. Инфраструктурная платформа на основе Kubernetes. Часть 2. Знакомство с Kubernetes: основные понятия и архитектура

--------------------------------------------------------

Сценарий работы с репозиторием:
```
cd existing_repo
git remote add origin git@gitlab.com:sb721407/kubernetes/k2.git
git branch -M main
git push -uf origin main
```

### Задание 1

https://gitlab.com/sb721407/kubernetes/k2/-/blob/main/pod2.yml


### Задание 2 

![Screenshot1](png/screen_1.png?raw=true "Screenshot #1")

